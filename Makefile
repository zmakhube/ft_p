# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/07/14 13:24:45 by zmakhube          #+#    #+#              #
#    Updated: 2017/09/08 14:07:56 by zmakhube         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#GENERIC
CC = gcc -Wall -Werror -Wextra
ECHO = echo
RED_L = \033[1;31m
GREEN = \033[0;32m
BLUE = \033[1;34m
RED = \033[0;31m
GRAY = \033[1;30m
CYAN = \033[0;36m
NC = \033[0m

#LIBFT
LIBFT = libft.a
LFLAGS = -L./libft/ -lft

#CLIENT VARS
_CLIENT = "client"
CLIENT_SRCS_DIR = ./srcs/client/
CLIENT_OBJS_DIR = ./objs/client/
CLIENT_SRC_FILES = client.c hostname.c transfer.c prompt.c commands.c utils.c \
					lput.c lget.c lcd.c local_io.c
CLIENT_OBJ_FILES = $(CLIENT_SRC_FILES:.c=.o)
CLIENT_OBJ = $(addprefix $(CLIENT_OBJS_DIR), $(CLIENT_OBJ_FILES))
CLIENT_INC = -I./srcs/client/include/ -I./libft/includes/

#SERVER VARS
_SERVER = "server"
SERVER_SRCS_DIR = ./srcs/server/
SERVER_OBJS_DIR = ./objs/server/
SERVER_SRC_FILES = server.c ls_pwd_commands.c cd_command.c get_command.c put_command.c server_utils.c
SERVER_OBJ_FILES = $(SERVER_SRC_FILES:.c=.o)
SERVER_OBJ = $(addprefix $(SERVER_OBJS_DIR), $(SERVER_OBJ_FILES))
SERVER_INC = -I./srcs/server/include/ -I./libft/includes/

#CREATE CLIENT OBJECTS DIRECTORY
$(CLIENT_OBJS_DIR):
	@mkdir -p $(CLIENT_OBJS_DIR)

#CREATE SERVER OBJECTS DIRECTORY
$(SERVER_OBJS_DIR):
	@mkdir -p $(SERVER_OBJS_DIR)

#COMPILE LIBFT
$(LIBFT):
	@make -C libft/

#COMPILE SERVER
$(_SERVER): $(SERVER_OBJ)
	@$(ECHO) "\n$(BLUE)Creating $(CYAN) $(_SERVER) $(BLUE)…\n"
	@$(CC) -o $@ $^ $(LFLAGS) $(SERVER_INC)
	@$(ECHO) "$(CYAN) $(_SERVER) $(GREEN) binary created.\n"

$(SERVER_OBJS_DIR)%.o: $(SERVER_SRCS_DIR)%.c
		@$(CC) $(SERVER_INC) -o $@ -c $<
		@$(ECHO) "$(BLUE) - $(GRAY)Compiling ==> $(RED)$<$(GRAY) ... $(GREEN)OK! $(NC)"

#COMPILE CLIENT
$(_CLIENT): $(CLIENT_OBJ)
	@$(ECHO) "\n$(BLUE)Creating $(CYAN) $(_CLIENT) $(BLUE)…\n"
	@$(CC) -o $@ $^ $(LFLAGS) $(CLIENT_INC)
	@$(ECHO) "$(CYAN) $(_CLIENT) $(GREEN) binary created.\n"

$(CLIENT_OBJS_DIR)%.o: $(CLIENT_SRCS_DIR)%.c
		@$(CC) $(CLIENT_INC) -o $@ -c $<
		@$(ECHO) "$(BLUE) - $(GRAY)Compiling ==> $(RED)$<$(GRAY) ... $(GREEN)OK! $(NC)"

#RULES
all: $(SERVER_OBJS_DIR) $(CLIENT_OBJS_DIR) $(LIBFT) $(_SERVER) $(_CLIENT)

server: $(SERVER_OBJS_DIR) $(LIBFT) $(_SERVER)

client: $(CLIENT_OBJS_DIR) $(LIBFT) $(_CLIENT)

clean:
	@$(ECHO) "$(RED)Removing object files ...$(NC)"
	@make -C libft/ clean
	@rm -rf $(SERVER_OBJS_DIR) $(CLIENT_OBJS_DIR)

fclean: clean
	@$(ECHO) "$(RED)Removing binary files => $(_SERVER)/$(_CLIENT) ...$(NC)"
	@rm -rf $(_SERVER) $(_CLIENT) $(SERVER_OBJ) $(CLIENT_OBJ)
	@make -C libft/ fclean

cclean: clean
	@$(ECHO) "$(RED)Removing binary files => $(_CLIENT) ...$(NC)"
	@rm -rf $(_CLIENT) $(CLIENT_OBJ)
	@make -C libft/ fclean

sclean: clean
	@$(ECHO) "$(RED)Removing binary files => $(_SERVER) ...$(NC)"
	@rm -rf $(_SERVER) $(SERVER_OBJ)
	@make -C libft/ fclean

re: fclean all
	@$(ECHO) "$(NC)\n"

srun: sclean server
	./$(_SERVER) 8888

crun: cclean client
	./$(_CLIENT) localhost 8888
