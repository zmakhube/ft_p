/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_pwd_commands.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 15:59:40 by zmakhube          #+#    #+#             */
/*   Updated: 2017/09/08 12:55:36 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static char    *sys_command(char *cmd)
{
    char    *newcmd;

    newcmd = ft_strnew(32);
    ft_memset(newcmd, '\0', 32);
    ft_strncat(newcmd, "/bin/", 5);
    ft_strncat(newcmd, cmd, ft_strlen(cmd));
    return (newcmd);
}

void    ls_pwd(int cfd, char **cmd)
{
    pid_t   pid;
    int     err;
    char    *command;

    err = 0;
    command = sys_command(cmd[0]);
    pid = fork();
    if (pid == 0)
    {
        dup2(cfd, 1);
        dup2(cfd, 2);
        execv(command, cmd);
    }
    else
    {
        wait4(pid, 0, 0, 0);
        if (command != NULL)
        {
            free(command);
        }
    }
}
