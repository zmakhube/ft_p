/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_command.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 15:13:38 by zmakhube          #+#    #+#             */
/*   Updated: 2017/09/15 18:15:49 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void send_file(int cs, int fd)
{
    char    buf[257];
    int     rbytes;

    rbytes = read(fd, buf, 256);
    while (rbytes > 0)
    {
        write(cs, buf, rbytes);
        ft_memset(buf, '\0', 257);
        rbytes = read(fd, buf, 256);
    }
}

static int get_filechecker(int fd, int cs)
{
    struct stat stats;

    fstat(fd, &stats);
    if (S_ISREG(stats.st_mode))
    {
        ft_putstr_fd("<RFFOUND>", cs);
        return (0);
    }
    else if (S_ISDIR(stats.st_mode))
        ft_putstr_fd("<RFISDIR>", cs);
    else
        ft_putstr_fd("<RFNFOUND>", cs);
    return (-1);
}

void       get(int cs, char **cmd)
{
    int fd;
    int check;

    fd = open(cmd[1], O_RDONLY);
    if (fd == -1)
    {
        ft_putstr_fd("<RFNFOUND>", fd);
        close(fd);        
        return ;
    }
    check = get_filechecker(cs, fd);
    if (check == 0)
    {
        send_file(cs, fd);
    }
    close(fd);
}
