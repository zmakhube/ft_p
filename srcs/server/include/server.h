/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/06 15:40:07 by zmakhube          #+#    #+#             */
/*   Updated: 2017/09/15 17:32:11 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVER_H
# define SERVER_H

# include "libft.h"
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include <sys/socket.h>
# include <sys/stat.h>
# include <arpa/inet.h>
# include <netdb.h>

void        ft_store_env(char **environ, char **env);
char	    *ft_getenv(char **env, char *var);
void	    ft_envdir(char **env, char *dir, char *data);
void        ls_pwd(int cs, char **cmd);
void        cd(int cs, char **cmd, char *rd, char **env);
void        get(int cs, char **cmd);
void        put(int cs, char **cmd);

#endif
