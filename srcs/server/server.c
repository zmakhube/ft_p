/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/06 15:38:38 by zmakhube          #+#    #+#             */
/*   Updated: 2017/09/08 14:58:55 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static int      init_server(const char *port, int sock_fd)
{
	struct sockaddr_in	server_addr;

	ft_memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_UNSPEC;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(ft_atoi(port));
    if (bind(sock_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
    {
        printf("ERROR failed to bind server socket to port %s\n", port);
        close(sock_fd);
        exit(1);
    }
    if (listen(sock_fd, 50) < 0)
    {
        close(sock_fd);
        printf("ERROR ftp server failed to start\n");
        exit(1);
    }
    else
    {
        printf("\n******************************************************\n");
        printf("\n*** WELCOME TO ZMAKHUBE'S FTP SERVER @WeThinkCode_ ***\n");
        printf("\n******************************************************\n");
    }
    return (sock_fd);
}

static void     process_command(int cs, char **cmd, char *rd, char **env)
{
    if (ft_strcmp(cmd[0], "ls") == 0 || ft_strcmp(cmd[0], "pwd") == 0)
    {
        ls_pwd(cs, cmd);
    }
    else if (ft_strcmp(cmd[0], "cd") == 0)
    {
        cd(cs, cmd, rd, env);
    }
    else if (ft_strcmp(cmd[0], "get") == 0)
    {
        get(cs, cmd);
    }
    else if (ft_strcmp(cmd[0], "put") == 0)
    {
        put(cs, cmd);
    }
}

static void     process_request(int cs, char *buf, char *home, char **env)
{
    char    **cmd;
    int     i;

    i = 0;
    cmd = ft_strsplit(buf, ' ');
    process_command(cs, cmd, home, env);
    free(cmd);
}

static int	    handle_connections(char *port, char *buffer, char *home,
    int sock_fd, char **env)
{
    struct sockaddr_in  c_addr;
    unsigned int        client_len;
    int                 cs;
    pid_t               cli;

    if ((sock_fd = init_server(port, sock_fd)) < 0)
    {
        printf("ERROR server failed to initialize\n");
        return (-1);
    }
    cli = fork();
    while (1)
    {
        if (cli == 0)
        {
            client_len = sizeof(c_addr);
            cs = accept(sock_fd, (struct sockaddr*)&c_addr, &client_len);
            ft_memset(buffer, 0, 1024);
            recv(cs, buffer, 1024, 0);
            process_request(cs, buffer, home, env);
        }
    }
	return (0);
}

int			    main(int ac, char **av, char **environ)
{
    char    *env[256];
    int     sock_fd;    
    char    buf[1024];
    char    home[256];

	if (ac != 2)
	{
		printf("USAGE >> ./server port\n");
		return (0);
    }
    ft_memset(home, '\0', 256);
    getcwd(home, 256);
    ft_store_env(environ, env);
    sock_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);    
	handle_connections(av[1], buf, home, sock_fd, env);
	return (0);
}
