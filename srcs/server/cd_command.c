/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd_command.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 15:12:27 by zmakhube          #+#    #+#             */
/*   Updated: 2017/09/08 14:38:34 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void	ft_subdir(char *dir, char *data)
{
	getcwd(dir, 256);
	ft_strncat(dir, "/", 1);
	ft_strncat(dir, data, ft_strlen(data));
}

static void	ft_prepend_homedir(char **env, char *dir, char *data)
{
	char	*tmp;

	tmp = ft_getenv(env, "HOME");
	ft_strncat(dir, tmp, ft_strlen(tmp));
	ft_strncat(dir, data, ft_strlen(data));
}

static void	change_dir(char *home, char *dir)
{
	char	cwd[256];

	if (chdir(dir) == 0)
	{
		ft_memset(cwd, '\0', 256);
		getcwd(cwd, 256);
		if (ft_strstr(cwd, home) == NULL)
		{
			chdir(home);
			printf("ERROR access to %s is forbidden.\n", cwd);
		}
		else
			printf("SUCCESS current directory is %s\n", cwd);
	}
	else
		printf("ERROR %s: no such file or directory\n", dir);
}

static void	ft_advanced_cd(int cfd, char **env, char *home, char *path, char *dir)
{
	dup2(cfd, 1);
	if (!path || ft_strcmp(path, "~") == 0 || ft_strcmp(path, "~/") == 0)
		ft_envdir(env, dir, "HOME");
    else if (*path == '$')
        ft_envdir(env, dir, path + 1);
	else if (*path == '~')
		ft_prepend_homedir(env, dir, path + 1);
	else if (*path == '/')
		ft_strncat(dir, path, ft_strlen(path));
	else
		ft_subdir(dir, path);
	change_dir(home, dir);
}

void		cd(int cfd, char **paths, char *home, char **env)
{
	char	*dir;

	dir = ft_strnew(256);
	ft_advanced_cd(cfd, env, home, paths[1], dir);
	free(dir);
}
