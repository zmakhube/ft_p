/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_command.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 15:14:34 by zmakhube          #+#    #+#             */
/*   Updated: 2017/08/15 15:14:49 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void upload_file(int cs, int fd)
{
    char    buf[257];
    int     rbytes;

    rbytes = read(cs, buf, 256);
    while (rbytes > 0 && buf[0] != '\0')
    {
        write(fd, buf, rbytes);
        ft_memset(buf, 0, 257);
        rbytes = read(cs, buf, 256);
    }
    close(fd);
}

static int  put_filechecker(int fd, int cs)
{
    struct stat stats;
    
    fstat(fd, &stats);
    if (fd == -1)
        return (0);
    if (S_ISREG(stats.st_mode))
        ft_putstr_fd("<FEXIST>", cs);
    else if (S_ISDIR(stats.st_mode))
        ft_putstr_fd("<FISDIR>", cs);
    close(fd);
    return (-1);
}

void    put(int cs, char **cmd)
{
    int fd;
    int check;

    fd = open(cmd[1], O_RDONLY);
    check = put_filechecker(fd, cs);
    if (check == 0)
    {
        close(fd);        
        fd = open(cmd[1], O_CREAT | O_WRONLY | O_EXCL, 0777);
        upload_file(cs, fd);
    }
}