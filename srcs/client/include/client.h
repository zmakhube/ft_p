/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/31 12:11:03 by zmakhube          #+#    #+#             */
/*   Updated: 2017/09/15 17:51:46 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLIENT_H
# define CLIENT_H

# include "libft.h"
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include <sys/socket.h>
# include <arpa/inet.h>
# include <netdb.h>
# include <sys/stat.h>

char    *ft_getIp(char *hostname);
void    ft_client_prompt(void);
void    ft_info_prompt(void);
void    ft_server_prompt(void);
int     send_request(int server_sock, char *cmd);
void    receive_response(int server_sock);
int     ft_check_command(char *input);
int     ft_validate_command(char *cmd);
void    ft_cmd_error(char *error, char *cmd);
void    ft_success(const char *data);
void    put(int server_sock, char *cmd, char *filename);
void    get(int server_sock, char *cmd, char *filename);
void    ft_store_env(char **environ, char **env);
char	*ft_getenv(char **env, char *var);
void	lcd(char **paths, char **env);
void    local_io(char *input, char **env);
void    ft_envdir(char **env, char *dir, char *data);

#endif
