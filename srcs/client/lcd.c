/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lcd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/08 13:40:13 by zmakhube          #+#    #+#             */
/*   Updated: 2017/09/08 14:34:50 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static void	ft_subdir(char *dir, char *data)
{
    getcwd(dir, 256);
    ft_strncat(dir, "/", 1);
    ft_strncat(dir, data, ft_strlen(data));
}

static void	ft_prepend_homedir(char **env, char *dir, char *data)
{
    char	*tmp;
    
    tmp = ft_getenv(env, "HOME");
    ft_strncat(dir, tmp, ft_strlen(tmp));
    ft_strncat(dir, data, ft_strlen(data));
}


static void	change_dir(char *dir)
{
    
    if (chdir(dir) == 0)
        printf("SUCCESS current directory is %s\n", dir);
    else
        printf("ERROR %s: no such file or directory\n", dir);
}

static void	ft_advanced_cd(char **env, char *path, char *dir)
{
    if (!path || ft_strcmp(path, "~") == 0 || ft_strcmp(path, "~/") == 0)
        ft_envdir(env, dir, "HOME");
    else if (*path == '$')
        ft_envdir(env, dir, path + 1);
    else if (*path == '~')
        ft_prepend_homedir(env, dir, path + 1);
    else if (*path == '/')
        ft_strncat(dir, path, ft_strlen(path));
    else
        ft_subdir(dir, path);
    change_dir(dir);
}

void		lcd(char **paths, char **env)
{
    char	*dir;
    
    dir = ft_strnew(256);
    ft_advanced_cd(env, paths[1], dir);
    free(dir);
}
