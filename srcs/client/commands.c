/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   commands.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/31 10:58:33 by zmakhube          #+#    #+#             */
/*   Updated: 2017/09/07 18:09:37 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

int         ft_validate_command(char *cmd)
{
    if (ft_strcmp(cmd, "ls") == 0)
        return (0);
    else if (ft_strcmp(cmd, "cd") == 0)
        return (0);
    else if (ft_strcmp(cmd, "pwd") == 0)
        return (0);
    else if (ft_strcmp(cmd, "get") == 0)
        return (1);
    else if (ft_strcmp(cmd, "put") == 0)
        return (2);
    else if (ft_strcmp(cmd, "lcd") == 0)
        return (3);
    else if (ft_strcmp(cmd, "lls") == 0)
        return (3);
    else if (ft_strcmp(cmd, "lpwd") == 0)
        return (3);
    else if (ft_strcmp(cmd, "quit") == 0)
        return (-1);
    return (-2);
}

int         ft_check_command(char *input)
{
    char    **data;
    int     valid;

    data = ft_strsplit(input, ' ');
    valid = ft_validate_command(data[0]);
    if (valid == -2)
    {
        ft_cmd_error(data[0], "is not a supported command");
    }
    return (valid);
}
