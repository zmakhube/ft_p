/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lput.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/06 21:14:42 by zmakhube          #+#    #+#             */
/*   Updated: 2017/09/07 17:47:41 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static void upload_file(int fd, int ss)
{
    int     rbytes;
    char    buf[257];

    rbytes = read(fd, buf, 256);
    while (rbytes > 0 && buf[0] != '\0')
    {
        write(ss, buf, rbytes);
        ft_memset(buf, 0, 257);
        rbytes = read(fd, buf, 256);
    }
}

static void upload_helper(int ss, int fd, char *res)
{
    struct stat stats;

    fstat(fd, &stats);
    if (ft_strcmp(res, "<FEXIST>") == 0)
        printf("ERROR file already exist with that name\n");
    else if (ft_strcmp(res, "<FISDIR>") == 0)
        printf("ERROR a directory already exist with that name\n");     
    else if (ft_strcmp(res, "<FISOK>") == 0 && S_ISREG(stats.st_mode))
        upload_file(fd, ss);
    else if (ft_strcmp(res, "<FISOK>") == 0 && S_ISDIR(stats.st_mode))
        printf("ERROR please upload a file instead of a directory\n");
}

void    put(int ss, char *cmd, char *filename)
{
    int     fd;
    char    res[12];

    fd = open(filename, O_RDONLY);
    if (fd == -1)
    {
        printf("ERROR %s not found\n", filename);
        close(fd);
        return ;
    }
    send_request(ss, cmd);
    ft_memset(res, '\0', 12);
    recv(ss, res, 12, 0);
    upload_helper(ss, fd, res);
    close(fd);
}
