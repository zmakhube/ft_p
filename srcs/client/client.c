/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/06 15:36:36 by zmakhube          #+#    #+#             */
/*   Updated: 2017/09/08 14:01:37 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static void get_input(char *input)
{
    read(0, input, 128);
    ft_strncat(input, "\0", 1);
}

static int  establish_communication(char *ip, char *s, char *port)
{
	struct sockaddr_in	s_addr;
    int                 server_sock;

	if ((server_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		ft_info_prompt();
		printf("ERROR socket failed to initialize\n");
		exit(0);
	}
	ft_memset(&s_addr, 0, sizeof(s_addr));
	s_addr.sin_family = AF_INET;
    s_addr.sin_addr.s_addr = inet_addr(ip);
	s_addr.sin_port = htons(ft_atoi(port));
    if (connect(server_sock, (struct sockaddr *)&s_addr, sizeof(s_addr)) < 0)
    {
		ft_info_prompt();
        printf("ERROR failed to connect to %s:%s\n", s, port);
        close(server_sock);
        exit(-1);
    }
	return (server_sock);
}

static void	remote_io(char *ip, int check, char *cmd, char *server, char *port)
{
    char    **data;
    int     server_sock;

    data = ft_strsplit(cmd, ' ');
    server_sock = establish_communication(ip, server, port);
	if (check == 0)
	{
		send_request(server_sock, cmd);
		receive_response(server_sock);
	}
	else if (check == 1)
		get(server_sock, cmd, data[1]);
    else if (check == 2)
        put(server_sock, cmd, data[1]);
}

static void process_input(char *ip, char *server, char *port, char **env)
{
	char	*cmd;
	int		check;

	cmd = ft_strnew(128);
	while (642)
	{
		get_input(cmd);
		if (cmd[ft_strlen(cmd) - 1] == '\n')
		{
			cmd[ft_strlen(cmd) - 1] = '\0';
			check = ft_check_command(cmd);
			if (check == 0 || check == 1 || check == 2)
				remote_io(ip, check, cmd, server, port);
            else if (check == 3)
                local_io(cmd, env);
            else if (check == -1)
            {
                exit (0);
            }
			ft_memset(cmd, '\0', 128);
			ft_client_prompt();
		}
	}
}

int			main(int ac, char **av, char **environ)
{
	char	*ip;
    char    *env[256];

	if (ac != 3)
	{
		printf("ERROR usage: ./client server port\n");
		return (0);
	}
	else
	{
		printf("\n***** ZMAKHUBE's FTP CLIENT *****\n\n");
		ft_info_prompt();
		printf("resolving %s ...\n", av[1]);
		ip = ft_getIp(av[1]);
		if (ft_strcmp(ip, "FAILURE") == 0)
			exit(0);
	}
	ft_client_prompt();
    ft_store_env(environ, env);
	process_input(ip, av[1], av[2], env);
	return (0);
}
