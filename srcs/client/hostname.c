/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hostname.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/17 13:30:08 by zmakhube          #+#    #+#             */
/*   Updated: 2017/07/17 13:30:20 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

char    *ft_getIp(char *hostname)
{
    struct hostent  *host_info;
    struct in_addr  **addr_list;
    char            *ip;

    ft_info_prompt();
    if ((host_info = gethostbyname(hostname)) == NULL)
    {
        printf("failed to resolve %s\n", hostname);
        return ("FAILURE");
    }
    addr_list = (struct in_addr **) host_info->h_addr_list;
    if (addr_list[0] != NULL) 
    {
        ip = inet_ntoa(*addr_list[0]);
        printf("%s resolved to %s\n", hostname, ip);    
        return (ip);
    }
    printf("failed to resolve %s\n", hostname);    
    return ("FAILURE");
}
