/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   local_cd.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/26 16:29:43 by zmakhube          #+#    #+#             */
/*   Updated: 2017/08/26 16:30:02 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static void	ft_envdir(char **env, char *dir, char *data)
{
	char	*tmp;

	tmp = ft_getenv(env, data);
	ft_strncat(dir, ft_getenv(env, data), ft_strlen(tmp));
}

static void	ft_subdir(char *dir, char *data)
{
	getcwd(dir, 128);
	ft_strncat(dir, "/", 1);
	ft_strncat(dir, data, ft_strlen(data));
}

static void	ft_prepend_homedir(char **env, char *dir, char *data)
{
	char	*tmp;

	tmp = ft_getenv(env, "HOME");
	ft_strncat(dir, tmp, ft_strlen(tmp));
	ft_strncat(dir, data, ft_strlen(data));
}

static void	ft_advanced_cd(char **env, char *path, char *dir)
{
	int	status;

	if (*path == '$')
		ft_envdir(env, dir, path + 1);
	else if (ft_strcmp(path, "~") == 0 || ft_strcmp(path, "~/") == 0)
		ft_envdir(env, dir, "HOME");
	else if (*path == '~')
		ft_prepend_homedir(env, dir, path + 1);
	else if (*path == '/')
		ft_strncat(dir, path, ft_strlen(path));
	else
		ft_subdir(dir, path);
	status = chdir(dir);
	if (status == 0)
		printf("SUCCESS Current Local directory is %s\n", dir);
	else
		printf("ERROR %s not found in this computer\n", dir);
	if (dir != NULL)
		free(dir);
}

void		lcd(char **paths, char **env)
{
	char	*dir;
	int i;

	i = 0;
	while (paths[i])
	{
		printf("PART: %s\n", paths[i]);
		i++;
	}
	return ;
	dir = ft_strnew(128);
	if (!paths[1])
	{
		ft_envdir(env, dir, "HOME");
	}
	ft_advanced_cd(env, paths[1], dir);
	return ;
}