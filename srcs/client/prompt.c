/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/17 15:29:28 by zmakhube          #+#    #+#             */
/*   Updated: 2017/07/17 15:29:30 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"


void    ft_client_prompt(void)
{
    ft_putstr_fd("\033[1;31mclient->\033[0m ", 0);
}

void    ft_info_prompt(void)
{
    ft_putstr_fd("\033[0;32m      ->\033[0m ", 0);
}

void    ft_server_prompt(void)
{
    ft_putstr_fd("\n\033[0;32mserver->\033[0m ", 0);
}
