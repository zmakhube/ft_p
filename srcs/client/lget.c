/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lget.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/06 21:13:58 by zmakhube          #+#    #+#             */
/*   Updated: 2017/09/15 18:07:38 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static void print_geterrors(int check)
{
    if (check == -4)
    {
        printf("ERROR the requested file exists in the current local directory."
                " Change the local directory using the lcd command\n");
    }
    else if (check == -3)
    {
        printf("ERROR you requested a directory instead of a file. Use ls "
                "command to view all files in the current remote directory\n");
    }
    else if (check == -2)
        printf("ERROR requested file not found in current remote directory\n");
    else if (check == -1)
    {
        printf("ERROR a directory with that name already exists in the current "
                "local directory. Use lls command to view all files in the "
                "current local directory\n");
    }
    else if (check == -5)
        printf("ERROR unexplainable error just occured\n");
}

static int  get_filechecker(int lfd, int ss, char *cmd)
{
    struct stat stats;
    char        res[11];

    fstat(lfd, &stats);
    if (S_ISREG(stats.st_mode))
        return (-4);
    else if (S_ISDIR(stats.st_mode))
        return (-1);
    ft_memset(res, 0, 11);
    send_request(ss, cmd);
    recv(ss, res, 11, 0);
    if (ft_strcmp(res, "<RFFOUND>") == 0)
        return (0);
    else if (ft_strcmp(res, "<RFISDIR>") == 0)
        return (-3);
    else if (ft_strcmp(res, "<RFNFOUND>") == 0)
        return (-2);
    return (-5);
}

static void get_file(int fd, int ss)
{
    char    buf[257];
    int     rbytes;

    rbytes = read(ss, buf, 256);
    while (rbytes > 0 && buf[0] != '\0')
    {
        write(fd, buf, rbytes);
        ft_memset(buf, 0, 257);
        rbytes = read(ss, buf, 256);
    }
    close(fd);
}

void        get(int ss, char *cmd, char *filename)
{
    int fd;
    int check;

    fd = open(filename, O_RDONLY);
    check = get_filechecker(lfd, ss, cmd);
    if (check < 0)
        print_geterrors(check);
    else if (check == 0)
    {
        close(fd);
        fd = open(filename, O_CREAT | O_WRONLY | O_EXCL, 0777);
        get_file(fd, ss);
    }
}
