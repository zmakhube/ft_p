/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/31 11:55:11 by zmakhube          #+#    #+#             */
/*   Updated: 2017/09/15 15:12:55 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

void    ft_cmd_error(char *cmd, char *error)
{
    printf("ERROR => %s %s\n", cmd, error);
}

void    ft_success(const char *data)
{
    char       *exist;
    const char *err = "ERROR";
    const char *suc = "SUCCESS";

    exist = ft_strstr(data, err);
    if (exist == NULL && ft_strstr(data, suc) == NULL)
    {
        ft_putstr_fd(data, 1);        
        ft_putstr_fd("SUCCESS\n", 1);
    }
    else
    {
        ft_putstr_fd(data, 1);        
    }
}

void    ft_store_env(char **environ, char **env)
{
    int     i;
    
    i = 0;
    while (environ[i] != NULL)
    {
        env[i] = ft_strnew(128);
        ft_memset(env[i], '\0', 128);
        ft_memcpy(env[i], environ[i], ft_strlen(environ[i]));
        i++;
    }
}

char	*ft_getenv(char **env, char *var)
{
    char	*tmp;
    char	*temp;
    int		i;
    
    i = 0;
    tmp = NULL;
    if (var != NULL)
    {
        temp = ft_strnew(128);
        ft_strncat(temp, var, ft_strlen(var));
        ft_strncat(temp, "=", 1);
        while (env[i] != NULL)
        {
            tmp = ft_strstr(env[i], temp);
            if (tmp != NULL)
            {
                free(temp);
                return (ft_strrchr(tmp, '=') + 1);
            }
            else
                i++;
        }
    }
    return (tmp);
}

void	ft_envdir(char **env, char *dir, char *data)
{
    char	*tmp;
    
    tmp = ft_getenv(env, data);
    ft_strncat(dir, ft_getenv(env, data), ft_strlen(tmp));
}
