/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   local_io.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 18:08:30 by zmakhube          #+#    #+#             */
/*   Updated: 2017/09/15 17:27:20 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static char *system_bin(char *command)
{
    char    *newcmd;

    newcmd = ft_strnew(32);
    ft_memset(newcmd, '\0', 32);
    ft_strncat(newcmd, "/bin/", 5);
    if (ft_strcmp(command, "lls") == 0)
        command = ft_strrchr(command, 'l');
    else if (ft_strcmp(command, "lpwd") == 0)
        command = ft_strchr(command, 'p');
    else if (ft_strcmp(command, "lcd") == 0)
    {
        command = ft_strchr(command, 'c');
        return (command);
    }
    ft_strncat(newcmd, command, ft_strlen(command));
    return (newcmd);
}

void        local_io(char *input, char **env)
{
    pid_t   pid;
    char    *command;
    char    **cmd;

    cmd = ft_strsplit(input, ' ');
    command = system_bin(cmd[0]);
    if (ft_strcmp(command, "cd") == 0)
    {
        lcd(cmd, env);
        return ;
    }
    pid = fork();
    if (pid == 0)
    {
        execv(command, cmd);
    }
    else
        wait4(pid, 0, 0, 0);
    if (command != NULL)
        free(command);
}
