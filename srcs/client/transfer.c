/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   transfer.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/17 14:16:17 by zmakhube          #+#    #+#             */
/*   Updated: 2017/07/17 14:16:35 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

int     send_request(int server_sock, char *cmd)
{
    int out;

    out = send(server_sock, cmd, (size_t)ft_strlen(cmd), 0);
    if (out == -1)
    {
        printf("ERROR => failed to send command (%s) to server ...\n", cmd);
    }
    return (server_sock);
}

void     receive_response(int server_sock)
{
    char    data[1024];

    ft_memset(data, 0, 1024);
    recv(server_sock, data, 1024, 0);
    ft_success(data);
}