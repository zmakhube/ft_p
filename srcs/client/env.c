/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/28 17:54:40 by zmakhube          #+#    #+#             */
/*   Updated: 2017/08/28 17:54:52 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

void    ft_store_env(char **environ, char **env)
{
	int     i;

	i = 0;
	while (environ[i] != NULL)
	{
		env[i] = ft_strnew(128);
		ft_memset(env[i], '\0', 128);
		ft_memcpy(env[i], environ[i], ft_strlen(environ[i]));
		i++;
	}
}

char	*ft_getenv(char **env, char *var)
{
	char	*tmp;
	char	*temp;
	int		i;

	i = 0;
	tmp = NULL;
	if (var != NULL)
	{
		temp = ft_strnew(128);
		ft_strncat(temp, var, ft_strlen(var));
		ft_strncat(temp, "=", 1);
		while (env[i] != NULL)
		{
			tmp = ft_strstr(env[i], temp);
			if (tmp != NULL)
			{
				free(temp);
				return (ft_strrchr(tmp, '=') + 1);
			}
			else
				i++;
		}
	}
	return (tmp);
}