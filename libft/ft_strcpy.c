/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/04 11:08:20 by zmakhube          #+#    #+#             */
/*   Updated: 2016/07/04 17:16:23 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

char	*ft_strcpy(char *dst, const char *src)
{
	size_t i;
	size_t size;

	i = -1;
	size = ft_strlen(src);
	while (++i <= size && *(src + i) != '\0')
	{
		*(dst + i) = *(src + i);
	}
	*(dst + i) = '\0';
	return (dst);
}
