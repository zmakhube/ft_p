/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/04 11:56:50 by zmakhube          #+#    #+#             */
/*   Updated: 2016/08/07 17:57:59 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

char	*ft_strncat(char *dst, const char *src, size_t n)
{
	size_t i;
	size_t dst_size;

	i = -1;
	dst_size = ft_strlen(dst);
	while (++i < n && *(src + i) != '\0')
	{
		*(dst + dst_size + i) = *(src + i);
	}
	*(dst + dst_size + i) = '\0';
	return (dst);
}
