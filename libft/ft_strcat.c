/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/04 11:37:01 by zmakhube          #+#    #+#             */
/*   Updated: 2016/07/04 17:11:56 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

char	*ft_strcat(char *dst, const char *src)
{
	size_t i;
	size_t dst_size;

	i = -1;
	dst_size = ft_strlen(dst);
	while (++i && *(src + i) != '\0')
	{
		*(dst + dst_size + i) = *(src + i);
	}
	*(dst + dst_size + i) = '\0';
	return (dst);
}
