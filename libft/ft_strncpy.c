/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/04 11:21:10 by zmakhube          #+#    #+#             */
/*   Updated: 2016/07/04 11:26:21 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

char	*ft_strncpy(char *dst, const char *src, size_t n)
{
	size_t i;

	i = -1;
	while (++i < n && *(src + i) != '\0')
	{
		*(dst + i) = *(src + i);
	}
	*(dst + i) = '\0';
	return (dst);
}
