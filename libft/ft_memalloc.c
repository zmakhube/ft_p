/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/04 11:27:07 by zmakhube          #+#    #+#             */
/*   Updated: 2016/07/04 17:11:11 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

void	*ft_memalloc(size_t size)
{
	void	*aloc;
	size_t	i;

	i = -1;
	aloc = malloc(sizeof(size_t) * size);
	if (!aloc)
		return (NULL);
	while (++i < size)
	{
		*((unsigned char *)aloc + i) = '\0';
	}
	return (aloc);
}
