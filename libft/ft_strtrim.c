/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zmakhube <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/18 09:45:37 by zmakhube          #+#    #+#             */
/*   Updated: 2017/05/18 09:45:50 by zmakhube         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

static int		ft_isesc(int c)
{
	if (c >= 7 && c <= 13)
		return (1);
	if (c == 32)
		return (1);
	return (0);
}

char			*ft_strtrim(char const *s)
{
	unsigned int	i;
	unsigned int	end;
	unsigned int	track;
	char			*tmp;

	if (s)
	{
		i = 0;
		end = ft_strlen(s) - 1;
		while (ft_isesc(s[i]))
			i++;
		if (!(tmp = (char *)malloc(sizeof(*tmp) * (end - i + 1))))
			return (NULL);
		while (ft_isesc(s[end]))
			end--;
		track = 0;
		while (i <= end)
		{
			tmp[track++] = s[i];
			i++;
		}
		tmp[track] = '\0';
		return (tmp);
	}
	return (NULL);
}
